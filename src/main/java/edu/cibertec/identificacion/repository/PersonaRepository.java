package edu.cibertec.identificacion.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import edu.cibertec.identificacion.entity.Persona;

@Repository
public interface PersonaRepository extends CrudRepository<Persona, Long> {

	Persona findByDni(String dni);
	Persona findByDniAndHuellaIzquierdaAndHuellaDerecha(String dni, String huellaIzquierda, String huellaDerecha);
}
