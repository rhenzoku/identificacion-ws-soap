package edu.cibertec.identificacion.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "MEJORES_HUELLAS")
public class MejorHuella {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@ManyToOne
	@JoinColumn(name="id_persona", nullable=false)
	private Persona persona;
	private String mano;
	private String dedo;
	
	public MejorHuella() {
	}

	public MejorHuella(Long id, String mano, String dedo, Persona persona) {
		super();
		this.id = id;
		this.mano = mano;
		this.dedo = dedo;
		this.persona = persona;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMano() {
		return mano;
	}

	public void setMano(String mano) {
		this.mano = mano;
	}

	public String getDedo() {
		return dedo;
	}

	public void setDedo(String dedo) {
		this.dedo = dedo;
	}

	public Persona getPersona() {
		return persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}

	@Override
	public String toString() {
		return "MejorHuella [id=" + id + ", persona=" + persona + ", mano=" + mano + ", dedo=" + dedo + "]";
	}
	
}
