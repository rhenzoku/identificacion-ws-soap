//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.3.2 
// Visite <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2020.03.08 a las 09:32:57 PM COT 
//


package edu.cibertec.identificacion.dto;

import java.util.Calendar;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Clase Java para persona complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="persona"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="dni" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="huellaDerecha" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="huellaIzquierda" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="fechaNacimiento" type="{http://www.w3.org/2001/XMLSchema}date"/&gt;
 *         &lt;element name="listaMejoresHuellas" type="{http://identificacion.cibertec.edu/identificacion}listaMejoresHuellas"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "persona", propOrder = {
    "dni",
    "huellaDerecha",
    "huellaIzquierda",
    "fechaNacimiento",
    "listaMejoresHuellas"
})
public class Persona {

    @XmlElement(required = true)
    protected String dni;
    @XmlElement(required = true)
    protected String huellaDerecha;
    @XmlElement(required = true)
    protected String huellaIzquierda;
    @XmlElement(required = true, type = String.class)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "date")
    protected Calendar fechaNacimiento;
    @XmlElement(required = true)
    protected ListaMejoresHuellas listaMejoresHuellas;

    /**
     * Obtiene el valor de la propiedad dni.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDni() {
        return dni;
    }

    /**
     * Define el valor de la propiedad dni.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDni(String value) {
        this.dni = value;
    }

    /**
     * Obtiene el valor de la propiedad huellaDerecha.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHuellaDerecha() {
        return huellaDerecha;
    }

    /**
     * Define el valor de la propiedad huellaDerecha.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHuellaDerecha(String value) {
        this.huellaDerecha = value;
    }

    /**
     * Obtiene el valor de la propiedad huellaIzquierda.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHuellaIzquierda() {
        return huellaIzquierda;
    }

    /**
     * Define el valor de la propiedad huellaIzquierda.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHuellaIzquierda(String value) {
        this.huellaIzquierda = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaNacimiento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getFechaNacimiento() {
        return fechaNacimiento;
    }

    /**
     * Define el valor de la propiedad fechaNacimiento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaNacimiento(Calendar value) {
        this.fechaNacimiento = value;
    }

    /**
     * Obtiene el valor de la propiedad listaMejoresHuellas.
     * 
     * @return
     *     possible object is
     *     {@link ListaMejoresHuellas }
     *     
     */
    public ListaMejoresHuellas getListaMejoresHuellas() {
        return listaMejoresHuellas;
    }

    /**
     * Define el valor de la propiedad listaMejoresHuellas.
     * 
     * @param value
     *     allowed object is
     *     {@link ListaMejoresHuellas }
     *     
     */
    public void setListaMejoresHuellas(ListaMejoresHuellas value) {
        this.listaMejoresHuellas = value;
    }

}
