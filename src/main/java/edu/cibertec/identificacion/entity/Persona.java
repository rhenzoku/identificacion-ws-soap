package edu.cibertec.identificacion.entity;

import java.util.Calendar;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "PERSONAS")
public class Persona {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String dni;
	private String huellaDerecha;
	private String huellaIzquierda;
	@Temporal(TemporalType.DATE)
	private Calendar fechaNacimiento;
	@OneToMany(
        mappedBy = "persona",
        fetch = FetchType.EAGER,
        cascade = CascadeType.ALL,
        orphanRemoval = true
    )
	private List<MejorHuella> listaMejoresHuellas;
	
	public Persona() {
	}
	
	public Persona(Long id, String dni, String huellaDerecha, String huellaIzquierda, Calendar fechaNacimiento,
			List<MejorHuella> listaMejoresHuellas) {
		super();
		this.id = id;
		this.dni = dni;
		this.huellaDerecha = huellaDerecha;
		this.huellaIzquierda = huellaIzquierda;
		this.fechaNacimiento = fechaNacimiento;
		this.listaMejoresHuellas = listaMejoresHuellas;
	}

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getDni() {
		return dni;
	}
	public void setDni(String dni) {
		this.dni = dni;
	}
	public String getHuellaDerecha() {
		return huellaDerecha;
	}
	public void setHuellaDerecha(String huellaDerecha) {
		this.huellaDerecha = huellaDerecha;
	}
	public String getHuellaIzquierda() {
		return huellaIzquierda;
	}
	public void setHuellaIzquierda(String huellaIzquierda) {
		this.huellaIzquierda = huellaIzquierda;
	}
	public Calendar getFechaNacimiento() {
		return fechaNacimiento;
	}
	public void setFechaNacimiento(Calendar fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}
	public List<MejorHuella> getListaMejoresHuellas() {
		return listaMejoresHuellas;
	}
	public void setListaMejoresHuellas(List<MejorHuella> listaMejoresHuellas) {
		this.listaMejoresHuellas = listaMejoresHuellas;
	}

	@Override
	public String toString() {
		return "Persona [id=" + id + ", dni=" + dni + ", huellaDerecha=" + huellaDerecha + ", huellaIzquierda="
				+ huellaIzquierda + ", fechaNacimiento=" + fechaNacimiento + ", listaMejoresHuellas="
				+ listaMejoresHuellas + "]";
	}
}
