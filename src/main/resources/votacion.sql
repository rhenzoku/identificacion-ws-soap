CREATE DATABASE votacion;
USE votacion;
SET GLOBAL time_zone = '+5:00';

drop table IF EXISTS personas;
drop table IF EXISTS mejores_huellas;

CREATE TABLE `personas` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `dni` varchar(255) DEFAULT NULL,
  `fecha_nacimiento` date DEFAULT NULL,
  `huella_derecha` varchar(255) DEFAULT NULL,
  `huella_izquierda` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ;

CREATE TABLE `mejores_huellas` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `dedo` varchar(255) DEFAULT NULL,
  `mano` varchar(255) DEFAULT NULL,
  `id_persona` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKrv54spcf2ho52jdjyon3tafut` (`id_persona`),
  CONSTRAINT `FKrv54spcf2ho52jdjyon3tafut` FOREIGN KEY (`id_persona`) REFERENCES `personas` (`id`)
) ;

INSERT INTO personas (DNI ,FECHA_NACIMIENTO ,HUELLA_DERECHA ,HUELLA_IZQUIERDA ) VALUES ('17245059','1990-12-15','SFVFTExBSU5ESUNFREVSRUNITzE3MjQ1MDU5','SFVFTExBSU5ESUNFSVpRVUlFUkRPMTcyNDUwNTk=');
INSERT INTO mejores_huellas (DEDO ,MANO ,ID_PERSONA ) VALUES ('indice','derecha',1);
INSERT INTO mejores_huellas (DEDO ,MANO ,ID_PERSONA ) VALUES ('pulgar','izquierdo',1);

INSERT INTO personas (DNI ,FECHA_NACIMIENTO ,HUELLA_DERECHA ,HUELLA_IZQUIERDA ) VALUES ('45687620','2003-06-01','SFVFTExBSU5ESUNFREVSRUNITzQ1Njg3NjIw','SFVFTExBSU5ESUNFSVpRVUlFUkRPNDU2ODc2MjA=');
INSERT INTO mejores_huellas (DEDO ,MANO ,ID_PERSONA ) VALUES ('anular','derecha',2);
INSERT INTO mejores_huellas (DEDO ,MANO ,ID_PERSONA ) VALUES ('medio','izquierdo',2);

INSERT INTO personas (DNI ,FECHA_NACIMIENTO ,HUELLA_DERECHA ,HUELLA_IZQUIERDA ) VALUES ('70082412','2003-07-01','SFVFTExBSU5ESUNFREVSRUNITzcwMDgyNDEy','SFVFTExBSU5ESUNFSVpRVUlFUkRPNzAwODI0MTI=');
INSERT INTO mejores_huellas (DEDO ,MANO ,ID_PERSONA ) VALUES ('meñique','derecha',3);
INSERT INTO mejores_huellas (DEDO ,MANO ,ID_PERSONA ) VALUES ('pulgar','izquierdo',3);