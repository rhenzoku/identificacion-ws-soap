package edu.cibertec.identificacion.service;

import edu.cibertec.identificacion.dto.MejorHuellaResponse;
import edu.cibertec.identificacion.dto.ValidarIdentidadResponse;

public interface PersonaService {
	
	public MejorHuellaResponse conseguirMejorHuella(String dni);
	public ValidarIdentidadResponse validarIdentidad(String dni, String huellaIzquierda, String huellaDerecha);

}
