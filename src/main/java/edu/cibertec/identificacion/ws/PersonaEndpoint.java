package edu.cibertec.identificacion.ws;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import edu.cibertec.identificacion.dto.MejorHuellaRequest;
import edu.cibertec.identificacion.dto.MejorHuellaResponse;
import edu.cibertec.identificacion.dto.ValidarIdentidadRequest;
import edu.cibertec.identificacion.dto.ValidarIdentidadResponse;
import edu.cibertec.identificacion.service.PersonaService;

@Endpoint
public class PersonaEndpoint {
	
	private static final String NAMESPACE_URI = "http://identificacion.cibertec.edu/identificacion";
	
	private PersonaService personaService;
	
	@Autowired
	public PersonaEndpoint(PersonaService personaService) {
		this.personaService = personaService;
	}
	
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "mejorHuellaRequest")
	@ResponsePayload
	public MejorHuellaResponse conseguirMejorHuella(@RequestPayload MejorHuellaRequest mejorHuellaRequest) {
		return personaService.conseguirMejorHuella(mejorHuellaRequest.getDni());
	}
	
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "validarIdentidadRequest")
	@ResponsePayload
	public ValidarIdentidadResponse validarIdentidad(@RequestPayload ValidarIdentidadRequest validarIdentidadRequest) {
		return personaService.validarIdentidad(
				validarIdentidadRequest.getPersona().getDni(), 
				validarIdentidadRequest.getPersona().getHuellaIzquierda(), 
				validarIdentidadRequest.getPersona().getHuellaDerecha());
	}
}
