package edu.cibertec.identificacion.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.cibertec.identificacion.dto.MejorHuellaResponse;
import edu.cibertec.identificacion.dto.ValidarIdentidadResponse;
import edu.cibertec.identificacion.repository.PersonaRepository;

@Service
public class BasicPersonaService implements PersonaService{
	
	private final Logger logger = LogManager.getLogger(BasicPersonaService.class);
	
	@Autowired
	PersonaRepository personaRepository;

	@Override
	public MejorHuellaResponse conseguirMejorHuella(String dni) {
		MejorHuellaResponse response = new MejorHuellaResponse();
		edu.cibertec.identificacion.dto.Persona persona = new edu.cibertec.identificacion.dto.Persona();
		
		try {
			edu.cibertec.identificacion.entity.Persona personaEntity = personaRepository.findByDni(dni);
			if(personaEntity != null) {
				mapearAtributosPersona(persona, personaEntity);
				response.setPersona(persona);
			}
			
		} catch (Exception e) {
			logger.error("::ERROR ",e);
		}
		
		return response;
	}

	@Override
	public ValidarIdentidadResponse validarIdentidad(String dni, String huellaIzquierda, String huellaDerecha) {

		ValidarIdentidadResponse response = new ValidarIdentidadResponse();
		
		edu.cibertec.identificacion.entity.Persona p = personaRepository.findByDniAndHuellaIzquierdaAndHuellaDerecha(
				dni, huellaIzquierda, huellaDerecha);
		
		if(p != null) {
			response.setValido(true);
		}else {
			response.setValido(false);
		}

		return response;
	}
	
	private void mapearAtributosPersona(
			edu.cibertec.identificacion.dto.Persona destino,
			edu.cibertec.identificacion.entity.Persona origen) {
		
		if (destino == null) {
            throw new IllegalArgumentException
                    ("El objeto destino no puede ser nulo");
        }
        if (origen == null) {
            throw new IllegalArgumentException("El objeto de origen no puede ser nulo");
        }
        
        destino.setDni(origen.getDni());
        destino.setFechaNacimiento(origen.getFechaNacimiento());
        destino.setHuellaDerecha(origen.getHuellaDerecha());
        destino.setHuellaIzquierda(origen.getHuellaIzquierda());
        origen.getListaMejoresHuellas();
        
		edu.cibertec.identificacion.dto.ListaMejoresHuellas listaMejoresHuellas = 
				new edu.cibertec.identificacion.dto.ListaMejoresHuellas();
		edu.cibertec.identificacion.dto.MejorHuella mejorHuella = null;
		
		for(edu.cibertec.identificacion.entity.MejorHuella mejorHuellaEntity:origen.getListaMejoresHuellas()) {
			mejorHuella = new edu.cibertec.identificacion.dto.MejorHuella();
			mejorHuella.setDedo(mejorHuellaEntity.getDedo());
			mejorHuella.setMano(mejorHuellaEntity.getMano());
			listaMejoresHuellas.getMejorHuella().add(mejorHuella);
		}
        destino.setListaMejoresHuellas(listaMejoresHuellas);
	}

}
