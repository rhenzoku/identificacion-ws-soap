package edu.cibertec.identificacion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IdentificacionWsSoapApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(IdentificacionWsSoapApplication.class, args);
	}
}
